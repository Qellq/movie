package com.example.phsyren.Movie4Today.HTTP;

/**
 * Created by Phsyren on 2016-03-16.
 */
public class UrlRequest {

    public static final String API_KEY = "api_key=6d6169eb545767e4865955fc68731c23";
    public static final String URL_API = "https://api.themoviedb.org/3/";

    public static final String URL_API_BEST_RATE = "movie/top_rated?api_key=6d6169eb545767e4865955fc68731c23&language=pl";

    public static final String URL_API_POPULAR = "discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=popularity.desc";
    public static final String URL_API_BEST_RATE_MOVIEWS_FORM_GENRES = "discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=vote_average.desc&vote_count.gte=100&with_genres=>>28<<";

    public static final String URL_API_GENRES = "https://api.themoviedb.org/3/genre/";
    public static final String URL_API_POPULAR_GENRES = ">>28<</movies?api_key=6d6169eb545767e4865955fc68731c23&language=pl";
    public static final String URL_API_GENRES_LIST = "list?api_key=6d6169eb545767e4865955fc68731c23&language=pl";
}
