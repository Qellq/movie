package com.example.phsyren.Movie4Today.Database;

/**
 * Created by Phsyren on 2016-03-21.
 */
public class DatabaseContract {

    public static class Movie {

        public static final String TABLE_NAME = "movie";
        public static final String _ID = "_id";
        public static final String MOVIE_ID = "movie_id";
        public static final String IMAGE_ID = "image_id";
        public static final String TITLE = "title";
        public static final String RELEASE_DATE = "release_date";
        public static final String ID_GENRES = "id_genres";
        public static final String GENRES = "genres";
        public static final String OVERVIEW = "overview";
        public static final String SERVICE_RATE = "service_rate";
        public static final String USER_RATE = "user_rate";
        public static final String FAVORITE = "favorite";
        public static final String isRate = "isRate";

    }
}
