package com.example.phsyren.Movie4Today.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phsyren on 2016-03-11.
 */
public class Movie implements Serializable {

    public static final String MOVIE_ID = "id";
    public static final String TITLE = "title";
    public static final String IMAGE = "";
    public static final String RELEASE_DATE = "release_date";
    public static final String GENRES_ID = "genre_ids";
    public static final String OVERVIEW = "overview";
    public static final String POPULARITY = "popularity";
    public static final String VOTE_AVERAGE = "vote_average";


   public int idMovie = 0;
   public long id = 0;
   public String poster_path = "";
   public String title = "";
   public String release_date = "";
   public ArrayList<Integer> genre_ids = new ArrayList<>();
   public String genres;
   public String overview = "";
   public double popularity = 0.0;
   public double vote_average = 0.0;
   public double userRating = 0.0;

    public int getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(int idMovie) {
        this.idMovie = idMovie;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setGenre_ids(ArrayList<Integer> genre_ids) {
        this.genre_ids = genre_ids;
    }

    public ArrayList<Integer> getGenre_ids() {
        return genre_ids;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String description) {
        this.overview = description;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(float vote_average) {
        this.vote_average = vote_average;
    }

    public double getUserRating() {
        return userRating;
    }

    public void setUserRating(float userRating) {
        this.userRating = userRating;
    }

    public Movie() {

    }

//    public Movie(Parcel in) {
//        this.idMovie = in.readInt();
//        this.id = in.readLong();
//        this.poster_path = in.readString();
//        this.title = in.readString();
//        this.release_date = in.readString();
//        this.genre_ids = in.readInt();
//        this.genres = in.readString();
//        this.overview = in.readString();
//        this.popularity = in.readFloat();
//        this.vote_average = in.readFloat();
//        this.userRating = in.readFloat();
//    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//
//        dest.writeInt(idMovie);
//        dest.writeLong(id);
//        dest.writeString(poster_path);
//        dest.writeString(title);
//        dest.writeString(release_date);
//        dest.writeInt(genre_ids);
//        dest.writeString(genres);
//        dest.writeString(overview);
//        dest.writeDouble(popularity);
//        dest.writeDouble(vote_average);
//        dest.writeDouble(userRating);
//
//    }
//
//    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
//        @Override
//        public Movie createFromParcel(Parcel in) {
//            return new Movie(in);
//        }
//
//        @Override
//        public Movie[] newArray(int size) {
//            return new Movie[size];
//        }
//    };

    public static Movie fromJsonObject(JSONObject object) throws JSONException, ParseException {
        Movie movie = new Movie();

        movie.id = object.getLong(MOVIE_ID);
        movie.title = object.getString(TITLE);
        movie.release_date = object.getString(RELEASE_DATE);
//        movie.genre_ids = object.getJSONArray(GENRES_ID);
        movie.overview = object.getString(OVERVIEW);
        movie.popularity = object.getDouble(POPULARITY);
        movie.vote_average = object.getDouble(VOTE_AVERAGE);

        return movie;
    }

    public static List<Movie> fromJsonArray (JSONArray jsonArray) throws JSONException, ParseException {

        List<Movie> movies = new ArrayList<Movie>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            Movie movie = Movie.fromJsonObject(object);
            movies.add(movie);
        }
        return movies;
    }

}
