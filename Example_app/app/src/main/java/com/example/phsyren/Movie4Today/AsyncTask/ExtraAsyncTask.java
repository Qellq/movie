package com.example.phsyren.Movie4Today.AsyncTask;

import android.os.AsyncTask;

/**
 * Created by Phsyren on 2016-03-30.
 */
public abstract class ExtraAsyncTask<Params, Result> extends AsyncTask<Params, Void, ResultAsyncTask<Result>> {

    @Override
    protected final ResultAsyncTask<Result> doInBackground(Params... params) {
        try {
            return new ResultAsyncTask<Result>(doInAnotherThread(params));
        } catch (Exception e) {
            return new ResultAsyncTask<Result>((Result) e);
        }
    }

    @Override
    protected final void onPostExecute(ResultAsyncTask<Result> resultResultAsyncTask) {
        onSuccess(resultResultAsyncTask.getResult());
    }

    protected void onSuccess(Result result) {
    }

    protected abstract Result doInAnotherThread(Params[] paramses) throws Exception;
}
