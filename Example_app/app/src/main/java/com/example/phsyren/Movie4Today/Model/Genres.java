package com.example.phsyren.Movie4Today.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phsyren on 2016-03-30.
 */
public class Genres implements Serializable {

    public static final String GENRES_ID = "id";
    public static final String GENRES_NAME = "name";

    public long id;
    public String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genres() {

    }

    public static Genres GenresfromJsonObject(JSONObject object) throws JSONException, ParseException {
        Genres genres = new Genres();

        genres.id = object.getLong(GENRES_ID);
        genres.name = object.getString(GENRES_NAME);

        return genres;
    }

    public static List<Genres> GenresfromJsonArray (JSONArray jsonArray) throws JSONException, ParseException {

        List<Genres> mGenres = new ArrayList<Genres>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            Genres genres = Genres.GenresfromJsonObject(object);
            mGenres.add(genres);
        }
        return mGenres;
    }

}
