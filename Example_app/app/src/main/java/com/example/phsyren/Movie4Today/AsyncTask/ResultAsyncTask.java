package com.example.phsyren.Movie4Today.AsyncTask;

/**
 * Created by Phsyren on 2016-03-30.
 */
public class ResultAsyncTask<T> {

    private T result;

    public ResultAsyncTask(T result) {
        this.result = result;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }



}
