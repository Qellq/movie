package com.example.phsyren.Movie4Today;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.phsyren.Movie4Today.Adapter.GenresAdapter;
import com.example.phsyren.Movie4Today.Adapter.MoviesAdapter;
import com.example.phsyren.Movie4Today.AsyncTask.ExtraAsyncTask;
import com.example.phsyren.Movie4Today.HTTP.HttpConnect;
import com.example.phsyren.Movie4Today.Model.Genres;
import com.example.phsyren.Movie4Today.Model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener {

    private Toolbar toolbar;
    private ListView listView;
    private ListView genresList;
    private Button previousBtn;
    private Button nextBtn;
    private TextView pageNumber;
    //private MoviesAdapter moviesAdapter;
    private GenresAdapter genresAdapter;
    private List<Genres> mGenres = new ArrayList<Genres>();
    //private List<Movie> movies = new ArrayList<Movie>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        previousBtn = (Button) findViewById(R.id.previous);
        nextBtn = (Button) findViewById(R.id.next);
        pageNumber = (TextView) findViewById(R.id.page_number);


//        listFragment = (ListFragment) getSupportFragmentManager().findFragmentByTag("fragmentMovieList");


//        moviesAdapter = new MoviesAdapter(this, R.layout.item_movie, movies);
        genresAdapter = new GenresAdapter(this, R.layout.item_genres, mGenres);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        new LoadGenres().execute("https://api.themoviedb.org/3/genre/list?api_key=6d6169eb545767e4865955fc68731c23&language=pl");

        genresList = (ListView) findViewById(R.id.genres_list);
        genresList.setAdapter(genresAdapter);

//        new LoadMovies().execute("https://api.themoviedb.org/3/discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=popularity.desc");

//        listView = (ListView) findViewById(R.id.movies_list);
//        listView.setAdapter(moviesAdapter);
//
        genresList.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, MainActivity.class);

        intent.putExtra(FragmentMovieList.EXTRA_MOVIE_LIST, genresAdapter.getItem(position));

        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    public class LoadMovies extends AsyncTask<String,Void,List<Movie>> {
//
//        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
//
//        @Override
//        protected void onPreExecute() {
//            this.progressDialog.setMessage("Download");
//            this.progressDialog.show();
//        }
//
//        @Override
//        protected List<Movie> doInBackground(String... params) {
//            try {
//
//                List<Movie> movies = new ArrayList<Movie>();
//                /**
//                 * pobieranie wszystkich filmów ze liczby stron
//                 */
////                String jsonPages = HttpConnect.run("https://api.themoviedb.org/3/discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=popularity.desc");
////                JSONObject objectPages = new JSONObject(jsonPages);
////                int total_pages = objectPages.getInt("total_pages");
////                for (int p = 1; p < 20; p++) {
//                // koniec
//
//                    String jsonData;
//                    jsonData = HttpConnect.run(params);
//                    JSONObject jsonObject = new JSONObject(jsonData);
//                    JSONArray jsonArray = jsonObject.getJSONArray("results");
//
//                    movies = Movie.fromJsonArray(jsonArray);
//
//                return movies;
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(List<Movie> movies) {
//            super.onPostExecute(movies);
//
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//
//            moviesAdapter.addAll(movies);
//        }
//
//    }

    public class LoadGenres extends AsyncTask<String ,Void, List<Genres>> {

        @Override
        protected List<Genres> doInBackground(String... params) {
            try {
                List<Genres> mGenres = new ArrayList<Genres>();
                /**
                 * pobieranie wszystkich filmów ze liczby stron
                 */
//                String jsonPages = HttpConnect.run("https://api.themoviedb.org/3/discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=popularity.desc");
//                JSONObject objectPages = new JSONObject(jsonPages);
//                int total_pages = objectPages.getInt("total_pages");
//                for (int p = 1; p < 20; p++) {
                // koniec

                String jsonData;
                jsonData = HttpConnect.run(params);
                JSONObject jsonObject = new JSONObject(jsonData);
                JSONArray jsonArray = jsonObject.getJSONArray("genres");

                mGenres = Genres.GenresfromJsonArray(jsonArray);

                return mGenres;

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Genres> mGenres) {
            super.onPostExecute(mGenres);

            genresAdapter.addAll(mGenres);
        }
    }
}
