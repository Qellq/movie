package com.example.phsyren.Movie4Today.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.phsyren.Movie4Today.Model.Movie;
import com.example.phsyren.Movie4Today.R;

import java.util.List;

/**
 * Created by Phsyren on 2016-03-11.
 */
public class MoviesAdapter extends ArrayAdapter<Movie> {

    private final LayoutInflater layoutInflater;

    public MoviesAdapter(Context context, int resource, List<Movie> objects) {
        super(context, resource, objects);
        layoutInflater = LayoutInflater.from(getContext());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_movie, null);
            viewHolder.movie_title = (TextView) convertView.findViewById(R.id.movie_title);
            viewHolder.release_date = (TextView) convertView.findViewById(R.id.release_date);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Movie movie = getItem(position);
        viewHolder.movie_title.setText(movie.getTitle());
        viewHolder.release_date.setText(movie.getRelease_date());
        viewHolder.release_date.setTextSize(14);

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    private static class ViewHolder {

        public TextView movie_title;
        public TextView release_date;
    }

}

