package com.example.phsyren.Movie4Today;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.phsyren.Movie4Today.Model.Movie;

/**
 * Created by Phsyren on 2016-03-12.
 */
public class MovieDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIE = "extra_movie";

    private Movie movie;
    private TextView title;
    private TextView release_date;
    private TextView overview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_movie_detail);

//        if (!getIntent().hasExtra(EXTRA_MOVIE)) {
//            throw new RuntimeException("Błąd");
//        }

        movie = (Movie) getIntent().getSerializableExtra(EXTRA_MOVIE);

        title = (TextView) findViewById(R.id.movie_title);
        release_date = (TextView) findViewById(R.id.movie_release_date);
        overview = (TextView) findViewById(R.id.movie_overview_container);

        title.setText(movie.getTitle());
        release_date.setText(movie.getRelease_date());
        overview.setText(movie.getOverview());

    }

}
