package com.example.phsyren.Movie4Today;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.phsyren.Movie4Today.Adapter.MoviesAdapter;
import com.example.phsyren.Movie4Today.HTTP.HttpConnect;
import com.example.phsyren.Movie4Today.Model.Item;
import com.example.phsyren.Movie4Today.Model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phsyren on 2016-03-30.
 */
public class FragmentMovieList extends ListFragment implements AdapterView.OnItemClickListener {

    public static final String EXTRA_MOVIE_LIST = "extra_movie_list";
    public MoviesAdapter moviesAdapter;
    public List<Movie> movies;

    public FragmentMovieList() {

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        new LoadMovies().execute("https://api.themoviedb.org/3/discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=popularity.desc");

        moviesAdapter = new MoviesAdapter(getActivity(),R.layout.item_movie, movies);

        setListAdapter(moviesAdapter);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);

        intent.putExtra(MovieDetailsActivity.EXTRA_MOVIE, moviesAdapter.getItem(position));

        startActivity(intent);
    }

    public class LoadMovies extends AsyncTask<String,Void,List<Movie>> {

        ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            this.progressDialog.setMessage("Download");
            this.progressDialog.show();
        }

        @Override
        protected List<Movie> doInBackground(String... params) {
            try {

                List<Movie> movies = new ArrayList<Movie>();
                /**
                 * pobieranie wszystkich filmów ze liczby stron
                 */
//                String jsonPages = HttpConnect.run("https://api.themoviedb.org/3/discover/movie?api_key=6d6169eb545767e4865955fc68731c23&language=pl&sort_by=popularity.desc");
//                JSONObject objectPages = new JSONObject(jsonPages);
//                int total_pages = objectPages.getInt("total_pages");
//                for (int p = 1; p < 20; p++) {
                // koniec

                String jsonData;
                jsonData = HttpConnect.run(params);
                JSONObject jsonObject = new JSONObject(jsonData);
                JSONArray jsonArray = jsonObject.getJSONArray("results");

                movies = Movie.fromJsonArray(jsonArray);

                return movies;

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Movie> movies) {
            super.onPostExecute(movies);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            moviesAdapter.addAll(movies);
        }

    }

    //    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//        moviesAdapter = new MoviesAdapter(getActivity(), R.layout.item_movie, movies);
//        View view = inflater.inflate(R.layout.fragment_list_movie, null, false);
//        listView = (ListView) view.findViewById(R.id);
//        listView.setAdapter(moviesAdapter);
//        listView.setOnItemClickListener(this);
//        return view;
//
//    }
//
//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//        Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
//
//        intent.putExtra(MovieDetailsActivity.EXTRA_MOVIE, moviesAdapter.getItem(position));
//
//        startActivity(intent);
//    }
}
